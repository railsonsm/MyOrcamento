package br.com.myorcamento.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String inicio() {
		System.out.println("teste");
		return "inicio";
	}
}
